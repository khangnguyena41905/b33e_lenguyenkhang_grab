// return money from kind off cars
UBER_CAR = "uberCar";
UBER_SUV = "uberSUV";
UBER_BLACK = "uberBlack";
function tinhGiaTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 10000;
  }
}
function tinhTienKm1_19(car) {
  switch (car) {
    case UBER_CAR:
      return 7500;
    case UBER_SUV:
      return 8500;
    case UBER_BLACK:
      return 9500;
    default:
      return 0;
  }
}
function tinhTienKm19TroDi(car) {
  switch (car) {
    case UBER_CAR:
      return 7000;
    case UBER_SUV:
      return 8000;
    case UBER_BLACK:
      return 9000;
    default:
      return 0;
  }
}

// main function
function tinhTienUber() {
  console.log("yes");
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
  // console.log("carOption: ", carOption);

  // var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
  // // console.log("giaTienKmDauTien: ", giaTienKmDauTien);
  // var test1_19 = tinhTienKm1_19(carOption);
  // // console.log("test1_19: ", test1_19);
  // var testTren19 = tinhTienKm19TroDi(carOption);
  // // console.log("testTren19: ", testTren19);
  var soKm = document.getElementById("txt-km").value * 1;
  // console.log("soKm: ", soKm);
  var tongSoTien = 0;
  // tinh tien
  if (soKm <= 1) {
    tongSoTien = soKm * tinhGiaTienKmDauTien(carOption);
  }
  if (soKm > 1 && soKm <= 19) {
    tongSoTien =
      tinhGiaTienKmDauTien(carOption) + (soKm - 1) * tinhTienKm1_19(carOption);
  }
  if (soKm > 19) {
    tongSoTien =
      tinhGiaTienKmDauTien(carOption) +
      18 * tinhTienKm1_19(carOption) +
      (soKm - 19) * tinhTienKm19TroDi(carOption);
  }
  // console.log("tong so tien: ", tongSoTien);
  document.getElementById("divThanhTien").classList.add("enable");
  document.getElementById("xuatTien").innerText = tongSoTien;
}
